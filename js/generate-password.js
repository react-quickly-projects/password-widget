const SPECIALS = '!@#$%^&*()_+{}:"<>?\|[]\',./`~'
const LOWERCASE = 'abcdefghijklmnopqrstuvwxyz'
const UPPERCASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
const NUMBERS = '0123456789'
const ALL = `${SPECIALS}${LOWERCASE}${UPPERCASE}${NUMBERS}`

const pick = (set, min, max) => {
    let length = min
    if(typeof max !== 'undefined')
        length += Math.floor(Math.random()*(max-min))
    let arr = []
    for(var i = 0; i < length; i++)
    {
        arr[i] = set.charAt(Math.floor(Math.random()*set.length))
    }
    return arr
}

const shuffle = (arr) =>
{
    for (let i = arr.length - 1; i > 0; i--)
    {
        const j = Math.floor(Math.random() * (i+1))
        let x = arr[i]
        arr[i] = arr[j]
        arr[j] = x
    }
    return arr
}

const generatePassword = function() {
    return shuffle([].concat(pick(SPECIALS, 1),
    pick(LOWERCASE, 1),
    pick(UPPERCASE, 1),
    pick(NUMBERS, 1),
    pick(ALL, 4, 12))).join('')
}

module.exports = generatePassword