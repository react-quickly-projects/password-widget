const generatePassword = require('../js/generate-password')
const pattern = /^[A-Za-z0-9\!\@\#\$\%\^\&\*\(\)\_\+\{\}\:\"\<\>\?\\|\[\]\/'\,\.\`\~]{8,16}$/

describe('method generatePassword', () => {
    let password, password2
    it('returns generated password of the set pattern', () => {
        password = generatePassword()
        expect(password).toMatch(pattern)
    })

    it('returns a different password upon subsequent call', () => {
        password2 = generatePassword()
        expect(password).not.toEqual(password2)
    })
})