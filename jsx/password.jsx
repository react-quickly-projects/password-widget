const React = require('react')
const PasswordInput = require('./password-input.jsx')
const PasswordVisibility = require('./password-visibility.jsx')
const PasswordGenerate = require('./password-generate.jsx')
const PasswordInfo = require('./password-info.jsx')
const generatePassword = require('../js/generate-password.js')

class Password extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            password: '',
            showPassword: false
        }
    }

    handlePasswordChange(event) {
        this.setState({ password: event.target.value })
    }

    handleShowPasswordClick(event) {
        this.setState({ showPassword: !this.state.showPassword })
    }

    handleGenerate() {
        this.setState({password: generatePassword()})
    }

    render() {
        const type = this.state.showPassword ? 'text' : 'password'
        return (
            <div>
                <PasswordInput password={this.state.password} showPassword={this.state.showPassword} handleChange={this.handlePasswordChange.bind(this)} />
                <br/>
                <PasswordVisibility checked={this.state.showPassword} handleClick={this.handleShowPasswordClick.bind(this)} />
                <br/>
                <PasswordInfo password={this.state.password}/>
                <br/>
                <PasswordGenerate onClick={this.handleGenerate.bind(this)}> Generate </PasswordGenerate>
            </div>
        )
    }
}

module.exports = Password