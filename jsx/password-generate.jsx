const React = require('react')

const PasswordGenerate = (props) => {
    return (
        <button {...props} className='btn generate-btn'>{props.children}</button>
    )
}

module.exports = PasswordGenerate
