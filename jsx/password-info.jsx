const React = require('react')
const rules = require('../js/rules.js')

const PasswordInfo = (props) => {
    const password = props.password

    return (
        <div>
            {Object.keys(rules).map((key, index) => {
                let style = {}
                //If password matches pattern
                if (rules[key].pattern.test(password)) {
                    style = { textDecoration: 'line-through' }
                }
                return (
                    <p key={index} style={style}>{rules[key].message}</p>
                )
            })}
        </div>
    )
}

module.exports = PasswordInfo