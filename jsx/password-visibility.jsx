const React = require('react')
module.exports = (props) => {
    return (
        <label>
            Show Password
            <input type='checkbox' checked={props.checked} onClick={props.handleClick} />
        </label>
    )
}