const React = require('react')


const PasswordInput = (props) => {
    const type = props.showPassword ? 'text' : 'password'
    return (
        <label>
            Password
            <input type={type} value={props.password} onChange={props.handleChange} />
        </label>
    )
}

module.exports = PasswordInput