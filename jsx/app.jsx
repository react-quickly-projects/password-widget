const React = require('react')
const ReactDOM = require('react-dom')
const Password = require('./password.jsx')

ReactDOM.render(
    <Password />,
    document.getElementById('password')
)
